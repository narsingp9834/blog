<?php

namespace Database\Seeders;

use App\Models\{Category, User, Post};
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    
        // insert default user 
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@yopmail.com',
                'password' => Hash::make('Password@1'),
                'role' =>   'admin',
                'valid' => 1
            ]
        ]);

        // insert tags 
        DB::table('tags')->insert([
            [
                'tag' => 'Tag 1', 
            ],
            [
                'tag' => 'Tag 2',
            ],
            [
                'tag' => 'Tag 3',
            ],
            [
                'tag' => 'Tag 4',
            ],
            [
                'tag' => 'Tag 5',
            ],
            [
                'tag' => 'Tag 6',
            ]
        ]);

    }

}
